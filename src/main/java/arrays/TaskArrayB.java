package arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TaskArrayB {
    public static void main(String[] args) {
        Integer[] one = {1,1, 2, 3, 2, 3, 4, 5, 6, 4, 8, 2, 9, 6, 3};
        List<Integer> list = new ArrayList<>(Arrays.asList(one));
        System.out.println(list);
        ArrayList<Integer> unique = new ArrayList<>();
        for (Integer number : list) {
            if (Collections.frequency(list, number) <= 2) {
                unique.add(number);
            }
        }
        System.out.println(unique);
    }


}
