package arrays.doorgame;

import arrays.doorgame.utils.PropertiesReader;

public class User {
    private int power= Integer.parseInt(PropertiesReader.readProperties("user_power"));

    public int getPower() {
        return power;
    }

    public void addPower(int power) {
        this.power += power;
    }
}
