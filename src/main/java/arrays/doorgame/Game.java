package arrays.doorgame;

import arrays.doorgame.door.Door;
import arrays.doorgame.door.DoorType;
import arrays.doorgame.utils.ConsoleReader;
import arrays.doorgame.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Game {
    private User user;
    private Hall hall;
    private View view;
    private ConsoleReader consoleReader;

    public Game(View view) {
        this.user = new User();
        this.hall = new Hall();
        this.view = view;
        this.consoleReader = new ConsoleReader(view);
    }

    public void startGame() {
        String value;
        do {
            value = consoleReader.scanFromConsole("MENU:\n1 - open doors\n2 - find strongest monsters" +
                    "\n3 - how to stay alive\n4 - exit");
            switch (value) {
                case "1":
                    openDoors();
                    break;
                case "2":
                    findStrongestMonsters();
                    break;
                case "3":
                    stayAlive();
                    break;
                default:
                    view.print("Wrong value");
            }
        } while (!value.equals("4"));
    }

    private void openDoors() {
        while (hall.isAllDoorsIsOpened()) {
            int value = consoleReader.readValue("Choose which door you want to open from 1 to 10", 10);
            Door door = hall.getDoor(value);
            if (!door.isOpen()) {
                if (door.getType().equals(DoorType.MONSTER)) {
                    if (user.getPower() >= door.getPower()) {
                        view.print("User is winner\nuser power: " + user.getPower() + "\nmonster power: "
                                + door.getPower());
                        door.setOpen(true);
                    } else {
                        view.print("Monster won");
                        break;
                    }

                } else {
                    user.addPower(door.getPower());
                    view.print("You open door with magic artifact and get " + door.getPower() + " power");
                    door.setOpen(true);
                }
            } else {
                view.print("You have already opened this door. Try another one!");
            }

        }
    }

    private void findStrongestMonsters() {
        List<Integer> list = new ArrayList<>();
        for (Map.Entry<Integer, Door> entry : hall.getDoors().entrySet()) {
            if (entry.getValue().getType().equals(DoorType.MONSTER) && entry.getValue().getPower() > user.getPower()) {
                list.add(entry.getKey());
            }
        }
        if (list.isEmpty()) {
            view.print("Not found strongest monsters than you");
        } else {
            view.print("Behind " + list.size() + " doors death waiting for you");
        }
    }

    private void stayAlive() {
        List<Integer> list = new ArrayList<>();
        for (Map.Entry<Integer, Door> entry : hall.getDoors().entrySet()) {
            if (entry.getValue().getType().equals(DoorType.MAGIC_ARTIFACT)) {
                list.add(entry.getKey());
            }
        }
        view.print("You need open this " + list + " doors to stay alive");
    }
}

