package arrays.doorgame.view;

public interface View {
    void print(String message);
    void print(int message);
}
