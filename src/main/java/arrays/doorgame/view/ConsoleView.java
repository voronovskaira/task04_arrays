package arrays.doorgame.view;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ConsoleView implements View {
    private static Logger LOG = LogManager.getLogger(ConsoleView.class);

    @Override
    public void print(String message) {
        LOG.info(message);
    }

    @Override
    public void print(int message) {
        LOG.info(message);
    }
}
