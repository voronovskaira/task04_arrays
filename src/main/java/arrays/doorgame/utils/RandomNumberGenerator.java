package arrays.doorgame.utils;

import java.util.Random;

public class RandomNumberGenerator {

    private Random random = new Random();

    public int getRandomNumb(int start, int end) {
        return random.nextInt(start) + end;
    }
}
