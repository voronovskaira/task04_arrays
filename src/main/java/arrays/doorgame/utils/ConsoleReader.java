package arrays.doorgame.utils;



import arrays.doorgame.view.View;

import java.util.Scanner;

public class ConsoleReader {

    private View view;

    public ConsoleReader(View view) {
        this.view = view;
    }

    public String scanFromConsole(String message) {
        Scanner scan = new Scanner(System.in);
        view.print(message);
        return scan.nextLine();
    }

    public int readValue(String message, int boarder) {
        try {
            int result = Integer.parseInt(scanFromConsole(message));
            if (checkRange(result, boarder))
                return result;
        } catch (NumberFormatException e) {
            view.print("Incorrect value. Try again");
        }
        return readValue(message, boarder);
    }


    private boolean checkRange(int value, int range) throws NumberFormatException {
        if (value > 1 && value <= range) {
            return true;
        } else throw new NumberFormatException();
    }


}
