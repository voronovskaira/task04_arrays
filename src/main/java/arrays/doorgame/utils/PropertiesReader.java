package arrays.doorgame.utils;



import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {
    private static String value;
    private static String FILE_PATH = "src/main/resources/config.properties";
    public static String readProperties(String key){
        Properties property = new Properties();
        try {
            FileInputStream file = new FileInputStream(FILE_PATH);
            property.load(file);
            value = property.getProperty(key);
        } catch (IOException e) {
            System.err.println("Error");
        }
        return value;
    }
}
