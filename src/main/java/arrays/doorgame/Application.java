package arrays.doorgame;

import arrays.doorgame.view.ConsoleView;

public class Application {
    public static void main(String[] args) {
        new Game(new ConsoleView()).startGame();
    }
}
