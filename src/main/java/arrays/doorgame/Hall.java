package arrays.doorgame;

import arrays.doorgame.door.Door;
import arrays.doorgame.door.DoorFabric;
import arrays.doorgame.utils.RandomNumberGenerator;

import java.util.HashMap;
import java.util.Map;

public class Hall {
    private Map<Integer, Door> doors;
    private RandomNumberGenerator random;
    private DoorFabric doorFabric;



    public Hall() {
        this.doors = new HashMap<>();
        this.random = new RandomNumberGenerator();
        this.doorFabric = new DoorFabric();
        generateDoors();
    }

    private void generateDoors() {
        for (int i = 1; i <= 10; i++) {
            doors.put(i, doorFabric.generateDoor(random.getRandomNumb(2,1)));
        }
    }

    public Door getDoor(int index) {
        return doors.get(index);
    }
    public boolean isAllDoorsIsOpened(){
        for(Door door : doors.values()){
            if(!door.isOpen())
                return true;
        }
        return false;
    }

    public Map<Integer, Door> getDoors() {
        return doors;
    }
}
