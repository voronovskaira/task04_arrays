package arrays.doorgame.door;

public class Door {
    private DoorType type;
    private int power;
    private boolean isOpen;

    public Door(DoorType type, int power) {
        this.type = type;
        this.power = power;
        this.isOpen = false;
    }

    public DoorType getType() {
        return type;
    }

    public int getPower() {
        return power;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }
}
