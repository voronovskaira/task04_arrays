package arrays.doorgame.door;

import arrays.doorgame.utils.PropertiesReader;
import arrays.doorgame.utils.RandomNumberGenerator;

public class DoorFabric {
    private RandomNumberGenerator numberGenerator = new RandomNumberGenerator();
    private Door generatedDoor;

    public Door generateDoor(int index) {
        if (index == 1)
            generatedDoor = new Door(DoorType.MONSTER, numberGenerator.getRandomNumb(
                    Integer.parseInt(PropertiesReader.readProperties("max_monster_power")),
                    Integer.parseInt(PropertiesReader.readProperties("min_monster_power"))));
        else if (index == 2)
            generatedDoor = new Door(DoorType.MAGIC_ARTIFACT, numberGenerator.getRandomNumb(
                    Integer.parseInt(PropertiesReader.readProperties("max_artifact_power")),
                    Integer.parseInt(PropertiesReader.readProperties("min_artifact_power"))));
        return generatedDoor;
    }

}
