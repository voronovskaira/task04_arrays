package arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskArrayC {
    private Integer[] array = {1, 2, 3, 2, 3, 3, 3, 4, 5, 6, 4, 4, 8, 2, 9, 6, 3, 3, 3};

    public void removeElements() {
        List<Integer> list = new ArrayList<>(Arrays.asList(array));
        System.out.println(list);
        List<Integer> newArray = new ArrayList<>();
        int previousNumber = 0;
        int currentNumber;
        for (Integer integer : list) {
            currentNumber = integer;
            if (previousNumber != currentNumber) {
                newArray.add(currentNumber);
                previousNumber = currentNumber;
            }
        }
        System.out.println(newArray);
    }
    public static void main(String[] args) {
        new TaskArrayC().removeElements();
    }
}
