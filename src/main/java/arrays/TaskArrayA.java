package arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskArrayA {
    private Integer[] firstArray = {1,1,7,2,4};
    private Integer[] secondArray = {1,2,4,5};

    private void arrayWithSameNumbers() {
        List<Integer> list = new ArrayList<>(Arrays.asList(firstArray));
        List<Integer> list1 = new ArrayList<>(Arrays.asList(secondArray));
        List<Integer> list2 = new ArrayList<>();
        for (Integer number : list) {
            if (list1.contains(number) && !list2.contains(number))
                list2.add(number);
        }
        System.out.println(list2);
    }

    private void arrayWithDifferentNumbers() {
        List<Integer> list = new ArrayList<>(Arrays.asList(firstArray));
        List<Integer> list1 = new ArrayList<>(Arrays.asList(secondArray));
        List<Integer> list2 = new ArrayList<>();
        for (Integer number : list) {
            if (!list1.contains(number) && !list2.contains(number))
                list2.add(number);
        }
        for (Integer number : list1) {
            if (!list.contains(number) && !list2.contains(number))
                list2.add(number);
        }
        System.out.println(list2);
    }


    public static void main(String[] args) {
        TaskArrayA array = new TaskArrayA();
        array.arrayWithDifferentNumbers();
        array.arrayWithSameNumbers();
    }
}
